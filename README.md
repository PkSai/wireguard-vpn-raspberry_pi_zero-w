# Raspberry Pi Zero W as A VPN Server

## Introduction
VPN stands for Virtual Private Network, it is a private network established over a public one. It can be thought of as a secure channel between two devices over a public network.
Many VPN services are available on the market claiming to provide user privacy but to truly achieve the "anonymity" they promise one's best bet would be to run a server themselves, this can be done in many ways from renting your own VPS (Virtual Private Server) to building your own server, the latter is focused on in this case. 

![image](https://gitlab.com/PkSai/wireguard-vpn-raspberry_pi_zero-w/-/raw/master/screenshots/intro.jpg)

## Requirements
1. A Raspberry Pi Zero-W.
2. A Micro-SD card with at least 8GB of storage and a class 10 speed rating.
3. A power brick with a 2.5A power supply.
4. An optional case to protect the hardware components.

## Setting up 
1. **Download** the Raspberry OS Lite image from the official [website](https://www.raspberrypi.org/software/operating-systems/) 
	-  Perform a shasum check
	```
		$ sha256sum zip.file
	```
	> The output from the command can be compared with the sha256 integrity hash available on the official Raspberry website.
	- Unzip the zip file.
2. **Flashing the ISO**
	- After connecting the SD card using an adapter or any other method, we can check if the system recognizes the SD card by using the ***lsblk*** command. An easy way to identify the card is to check the storage capacity. After the SD card is recognized we can simply flash the ISO using a simple linux command line tool called **dd**:
	```
		# dd if=/path/to/image/2021-01-11-raspios-buster-armhf-lite.img of=/dev/mmcblk0 bs=4M; status=progress
	```
	- if - stands for the input file, it will be the image file we unzipped eaerlier
	- of - is the output which will be our memory card
	- bs - is the block size
	- status - is used to monitor the status of the process
3. **Headless SSH**: Since this will be a headless setup, we will need to configure the Pi in a way that it would connect ot our wireless network at first boot, and also accept connections via SSH.
	- We will need to create a file called wpa_supplicant.conf:
	```
		$ vim wpa_supplicant.conf
	```
	The contents should contain the following:
	```
		ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
		update_config=1
		country=IN
		network={
			ssid="YourSSID"    
			psk="YourPassword"
			scan_ssid=1
		}
	```
	The file will be delete after boot, this is a one time thing, if you reboot now you will have to create the files once more.
	- Create a file called ssh without an extension, it may be empty or otherwise this won't matter.

4. **Booting up the Pi Zero-W:** After creating the files, we eject the Micro SD card from our machine and insert it into the Rasberry Pi Zero-W. We connect the Pi to a power source and wait for the it to boot up and connect to our wireless network.

5. **SSH into the Pi:** This can be done in multiple ways, you can use the ssh command in any linux terminal or the open-source application called putty.
	```
		$ ssh pi@<IP>
	```
	IP is the ip address of your raspberry pi. In the case that the IP address is unknown, the command line utility nmap can be used to scan the entire subnet:
	```
		# nmap -sn [device subnet]
	```
	Device subnet can be determined using the ip addr command or its equivalent in a Linux Environment.
	```
		$ ip addr
	``` 
	The nmap command is ran as root so as to display the device manufacturer and MAC address for easier identification.

6. **After connecting via SSH:**
	- Changing the default password for security:
		```
			$ sudo raspi-config
		```
	- Select "System Options", navigate to "Change User 'pi' password", supply it with the password of your choice and click "Finish"
	- Update the system using the command:
	```
		# apt update && apt upgrade
	```

7. **Installating and configuring wireguard**:
Rationale for using wireguard over OpenVPN and IPsec
- [Benchmarks](https://www.wireguard.com/performance/) show that wireguard far outclasses OpenVPN in terms of speed as well as IPsec.
- Wireguard, as described by the developers on their site, "is not a chatty protocol", therefore battery improvements will be more evident using this protocol over others.
- Wireguard will create a virtual interface present on both the server and client. Wireguard sends the request to the DDNS provider and it forwards this request to the Raspberry Pi.
	- Installing WireGuard
		```
			# apt install wireguard
		```
	- Generating security keys

		```
			$ su
			# cd /etc/wireguard
			# umask 077
			# wg genkey | tee server_private_key | wg pubkey > server_public_key
			# wg genkey | tee client_private_key | wg pubkey > client_public_key
		```
	- Storing the keys for refrence later

		``` 
			# cat server_private_key server_public_key client_private_key client_public_key > Keys

		```
	- Genereating a config file
		```
			# vi wg_pg1.conf
		```
	- Any name can be give, for the contents copy the following, we are considering wifi therefore we are using -o wlan0 in the PostUp and PostDown
	```
		[Interface]
		Address = 10.253.x.x/24
		SaveConfig = true
		PrivateKey = <insert the server private key from before> 
		ListenPort = 51900

		PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
		PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o wlan0 -j MASQUERADE

		[Peer]
		PublicKey = <insert the client public key from before> 
		AllowedIPs = 10.253.x.x/32

	```
	- Enabling IP forwarding on you raspberry server
	```
		# vi /etc/sysctl.conf
	```
	Uncomment "net.ipv4.ip_forward=1"

	- Configuring autostart at boot for WireGuard
	```
		# systemctl enable wg-quick@wg0
		# chown -R root:root /etc/wireguard/
		# chmod -R og-rwx /etc/wireguard/*
	```
	- Reboot the Pi 
	```
		# reboot
	```
7. Creating the WireGuard configuration file for the client device, this can be done on the client end.
	```
		$ vi wg_pg1_client.conf
	```
	The contents should contain

	```
		[Interface]
		Address = 10.253.x.x/32
		PrivateKey = <client private key>
		DNS = 1.1.1.1
		
		[Peer]
		PublicKey = <server public key>
		Endpoint = <insert the vpn_server_address>:51900
		AllowedIPs = 0.0.0.0/0, ::/0
	```
	This config file can be copied to any device you choose to use wireguard on, in linux copy the config file to the /etc/wireguard directory or its equivalent, the DDNS URL used i cloudflares server.

## ------------------------------
## For Linux  
8. Connecting from client: Install wireguard
	- If debian based
		```
			# apt install wireguard
		```
	- If arch based
		```
			# pacman -S wireguard
		```
	- Make sure that the config file is in /etc/wireguard or its equivalent.
	- Bring up the interface with:
		```
			# wg-quick up wg_pg1_client.conf
		```
	> If you used a different name for your config file, edit the command accordingly.
	- Connecting and disconnecting
	> Connecting
		```
			# wg-quick up wg_pg1_client.conf
		``` 
![image](https://gitlab.com/PkSai/wireguard-vpn-raspberry_pi_zero-w/-/raw/master/screenshots/wg-up.png)

	> Disconnecting
		```
			# wg-quick down wg_pg1_client.conf
		``` 
![image](https://gitlab.com/PkSai/wireguard-vpn-raspberry_pi_zero-w/-/raw/master/screenshots/wg-down.png)

	> Checking status
	```	
		# wg
	```
![image](https://gitlab.com/PkSai/wireguard-vpn-raspberry_pi_zero-w/-/raw/master/screenshots/wg.png)
## -------------------------------
## For android
8. **Connecting from client**:
	- Download the wireguard app, via the f-droid store.
	- Click the plus icon and select import file, select the config file that you created in this case it is 'wg_pg1_client.conf'. You should prompted for a confirmation and select 'allow'.
	
> Something like this will show up
![image](https://gitlab.com/PkSai/wireguard-vpn-raspberry_pi_zero-w/-/raw/master/screenshots/mobile.png)

## Conclusion
A Virtual Private Network may not provide the anonymity people think it does but setting up your own server is the closest one can achieve using this technology. This guide is a proof-of-concept that can be used on a real machine.
